from pylib.googleadmin import authService
from decouple import config
escopos = [
    "https://www.googleapis.com/auth/admin.directory.user"
]

def getAdminService(email=False):
    inpersonated = email if email else config('IMPERSONATED_EMAIL')
    return authService(
            escopos,
            "cred/{}".format(config("JSONFILE")),
            inpersonated #substituir pelo caminho do arquivo, cred/arquadasds.json
        ).getService("admin","directory_v1")

def criarUsuario(**kwargs):
    """ CRIA CONTA DO USUÁRIO NO GSUITE """
    # givenName
    # familyName
    # primaryEmail
    # password
    # documento
    # curso
    # campus

    service = getAdminService()
    body ={
        "name":{
            "givenName":kwargs.get("givenName"),
            "familyName":kwargs.get("familyName")
        },
        "primaryEmail":kwargs.get("primaryEmail"),
        "changePasswordAtNextLogin":"true",
        "password":kwargs.get("password"),
        "orgUnitPath":"/teste",
        "externalIds":[{
            "value":kwargs.get("documento"),
            "type":"organization"
        }],
        "organizations":[
            {
            "department":kwargs.get("curso"),
            "costCenter":kwargs.get("campus")
            }
        ]
    }

    try:
        usuario = service.users().insert(body=body).execute()
        return usuario
        print(usuario)
    except Exception as e:
        print(e)
        return False

def suspenderUsuario(userKey,body):
    """ Suspende usuário no GSuite """

    service = getAdminService()
    try:
        r = service.users().update(
            userKey=userKey,
            body=body
        ).execute()
        return r
    except Exception as e:
        print(e)
        raise Exception(e)