from pylib.googleadmin import authService
from googleapiclient import errors
from decouple import config
import datetime
escopos = [
    'https://www.googleapis.com/auth/admin.reports.audit.readonly',
    'https://www.googleapis.com/auth/admin.reports.usage.readonly'
    ]

def iniciarServiceReports(email=False):

    servico = (
        authService(
            escopos,
            "cred/{}".format(config("JSONFILE")),
            email
        ) 
        if email else 
        authService(
            escopos,
            "cred/{}".format(config("JSONFILE")),
            config("IMPERSONATED_EMAIL"))
        )
    return servico.getService("admin","reports_v1")

def carregarAtividadesPorUsuario(**kwargs):
    """ Carrega relatórios do Google Workspace por usuário """
    # userKey (email ou id),
    # applicationName
    servico = iniciarServiceReports()

    try:
        report = servico.activities().list(
            userKey=kwargs.get("userKey","all"),
            applicationName=kwargs.get("applicationName",None)
        ).execute()

        report
        
    except Exception as e:
        print(e)

def carregarMetricasPorEventos(**kwargs):
    """ Carrega relatórios quantitativos com base em métricas por aplicativo """
    # parametros ['account:num_users','...']
    # dias


    servico = iniciarServiceReports()
    data_alvo = datetime.date.today() - datetime.timedelta(days=kwargs.get('dias'))
    data_alvo = data_alvo.strftime("%Y-%m-%d")
    try:

        r = servico.customerUsageReports().get(
            date=data_alvo,
            parameters= ",".join(kwargs.get("parametros",None))
        ).execute()
        return r.get("usageReports",False)
    except errors.HttpError as e:
        print("Erro da api")
        print(e)
    except Exception as er:
        print("Erro de sistema")
        print(er)