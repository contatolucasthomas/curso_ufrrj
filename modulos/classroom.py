from pylib.googleadmin import authService
from decouple import config

escopos = [
    "https://www.googleapis.com/auth/classroom.courses",
    "https://www.googleapis.com/auth/classroom.rosters",
    "https://www.googleapis.com/auth/classroom.profile.emails",
    "https://www.googleapis.com/auth/classroom.profile.photos",
    "https://www.googleapis.com/auth/classroom.coursework.students",
]

def iniciarServicoClassroom(email=False):
    inpersonated = email if email else config('IMPERSONATED_EMAIL')
    return authService(
            escopos,
            "cred/{}".format(config("JSONFILE")),
            inpersonated #substituir pelo caminho do arquivo, cred/arquadasds.json
        ).getService("classroom","v1")

def criarAtividade(idturma,body,professor):
    servico = iniciarServicoClassroom(professor)
    try:
        atividade = servico.courses().courseWork().create(
            courseId=idturma,
            body=body
        ).execute()
        return atividade
    except Exception as e:
        print(e)
    pass

def carregarTodasTurmas():
    servico = iniciarServicoClassroom()
    todasturmas = []

    turmas = servico.courses().list(pageSize=500).execute()

    return turmas.get("courses",False) 

def ensalarAluno(id_turma_gsuite,body):

    servico = iniciarServicoClassroom()
    try:
        servico.courses().students().create(
            courseId=id_turma_gsuite,
            body=body
        ).execute()
        return True
    except Exception as e:
        raise Exception(e)

def criarTurma(**kwargs):
    classroom = iniciarServicoClassroom()
    # admin = getAdminService()
    body ={
        "name": kwargs.get("name","Sem nome"),
        "section": kwargs.get("section","Sem sessão"),
        "courseState": "ACTIVE",
        "ownerId": kwargs.get("ownerId","Sem nome"),
        "id":"d:{}".format(kwargs.get("id",""))
    }

    try:
        r = classroom.courses().create( body=body ).execute()
        return r

    except Exception as e:
        print(e)
        return False

