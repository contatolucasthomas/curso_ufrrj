import psycopg2
from configbanco import config
try:
    c = config()
    conn = psycopg2.connect(**c)
    cursor = conn.cursor()
except Exception as e:
    raise 

def criarTabelaLogUsuarios():
    commands = (
        """
            CREATE TABLE usuarios_criados (
                user_id SERIAL PRIMARY KEY,
                user_email VARCHAR(255) NOT NULL,
                user_gsuiteid VARCHAR(255) NOT NULL
            )
        """)
    cursor.execute(commands)
    cursor.close()
    conn.commit()

def logUsuariosNovos(id,email):
    cursor = conn.cursor()
    cursor.execute("INSERT INTO usuarios_criados (user_gsuiteid, user_email) VALUES(%s, %s)", (id, email))
    conn.commit() 
    cursor.close()
    conn.close()

if __name__ == '__main__':
    # criarTabelaLogUsuarios()
    logUsuariosNovos('13112313123','lucas@lucas.com')

