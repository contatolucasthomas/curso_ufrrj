from pylib.googleadmin import authService
from decouple import config

escopos = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.appdata',
    'https://www.googleapis.com/auth/drive.file',
    "https://www.googleapis.com/auth/drive.metadata",
    "https://www.googleapis.com/auth/documents",
]
def iniciarServicoDrive(email=False):
    inpersonated = email if email else config('IMPERSONATED_EMAIL')
    return authService(
            escopos,
            "cred/{}".format(config("JSONFILE")),
            inpersonated #substituir pelo caminho do arquivo, cred/arquadasds.json
        ).getService("drive","v3")

def criarNoDrive(**kwargs):
    """ Cria qualquer tipo de arquivo no drive, inclusive pastas """ 
    # name
    # mimeType 

    service = iniciarServicoDrive()
    parentId = '' #some parentId of a folder under which to create the new folder

    try:
        if not kwargs.get('parents',False):
            fileMetadata ={
                "mimeType": kwargs.get('mimeType'),
                "name": kwargs.get("name","Erro") #,
            }
        else:
            fileMetadata ={
                "mimeType": kwargs.get('mimeType'),
                "name": kwargs.get("name","Erro") ,
                "parents": kwargs.get('parents')
            }

        novoarquivo = service.files().create(body=fileMetadata).execute()
        return novoarquivo
    except Exception as e:
        print(e)
        return False

def listarArquivos(q=False):
    # q= "name = '{}' and mimeType = 'application/vnd.google-apps.form' "
    service = iniciarServicoDrive()
    if q:
        arquivos = service.files().list(
            fields="nextPageToken,files(*)", #o que eu quer ler
            pageSize=10,
            q=q
        ).execute()
    else:
        arquivos = service.files().list(
            fields="nextPageToken,files(*)", #o que eu quer ler
            pageSize=10
        ).execute()

    if 'files' not in arquivos:
        return False
    return arquivos['files']

    arquivo = service.files().get(fileId=file_id).execute()
