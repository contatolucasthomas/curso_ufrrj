from pylib.googleadmin import authService
from decouple import config

escopos = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.appdata',
    'https://www.googleapis.com/auth/drive.file',
    "https://www.googleapis.com/auth/drive.metadata",
    "https://www.googleapis.com/auth/documents",
]
def iniciarServicoDocs(email=False):
    inpersonated = email if email else config('IMPERSONATED_EMAIL')
    return authService(
            escopos,
            "cred/{}".format(config("JSONFILE")),
            inpersonated #substituir pelo caminho do arquivo, cred/arquadasds.json
        ).getService("docs","v1")

def escreverDocumento(**kwargs):
    """ Escreve conteúdo dentro de um determinado documento """ 
    # driveId
    # body
    service = iniciarServicoDocs()

    result = service.documents().batchUpdate(
        documentId=kwargs.get("driveId"), 
        body={'requests': kwargs.get("requests") } 
    ).execute()

    result