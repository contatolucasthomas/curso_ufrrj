# curso_ufrrj


# Referências
### Classroom:
- Criação de turmas: https://developers.google.com/classroom/reference/rest/v1/courses/create
- Ensalamento de aluno: https://developers.google.com/classroom/reference/rest/v1/courses.students/create

### Admin:
- https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/insert

### Drive:
- https://developers.google.com/drive/api/v3/reference/files
- https://developers.google.com/drive/api/v3/mime-types
- https://developers.google.com/drive/api/v3/reference/files/create

# Exemplos de como usar:

## Classroom
### Importar: 
- `from modulos.classroom import iniciarServicoClassroom, criarTurma, ensalarAluno, carregarTodasTurmas` 
- `from pylib.pycsv import PyCsv`
### Criar turma:
    turma = criarTurma(
        name='Turma Alias',
        section='2021',
        ownerId='bot.script@ufrrj.br',
        id='684468464684896'
    )

### Carregar todas as turmas do gsuite:
    turmas = carregarTodasTurmas()
    if turmas:
        for turma in turmas:
            csvTurmas.add_row_csv([
                turma.get('id'),
                turma.get('name'),
                turma.get('ownerId')
            ])
    else:
        print("Erro")

### Ensalar aluno:
    turmascriadas = PyCsv('logs/turmasCriadas').get_content()
    alunoscriados = PyCsv('logs/contasCriadas').get_content()

    for aluno in alunoscriados:

        aluno_email = aluno[2]
        
        for turma in turmascriadas:
            nometurma = turma[0]
            id_turma_gsuite = turma[1]
            body ={
                "courseId": id_turma_gsuite,
                "userId":aluno_email
                }
            try:
                r = ensalarAluno(id_turma_gsuite,body)
                loggerEnsalamentos.add_row_csv([
                    "Sucesso",
                    id_turma_gsuite,
                    nometurma,
                    aluno_email
                ])
            except Exception as e:
                print(e)
                loggerEnsalamentos.add_row_csv([
                    "Erro",
                    id_turma_gsuite,
                    nometurma,
                    aluno_email,
                    str(e)
                ])

### Criar conteúdo no classroom:
- importar : `from modulos.classroom import criarAtividade` 
- https://developers.google.com/classroom/reference/rest/v1/courses.courseWork/create

    ```
        for turma in turmas:
            idturma = turma[1]
            body={
                "title": "Atividade com novo link do drive",
                "assigneeMode": "INDIVIDUAL_STUDENTS",
                "maxPoints": 10,
                "state": "PUBLISHED",
                "description": "Testando",
                "workType": "ASSIGNMENT",
                "materials": [
                        {
                        "link": {
                            "title": "Link do porvir",
                            "url": "https://docs.google.com/document/d/1qX7RwtSSQ-sMWZhlHdQasswpYij0ZQHFhEGAMBSG9QM/edit?usp=sharing"
                        }
                        }
                    ]
                }
            professor = "bot.script@ufrrj.br"
            criarAtividade(idturma,body,professor)
    ```


## Admin SDK:
### Importar:
- `from modulos.admin import getAdminService, criarUsuario`
- `from pylib.pycsv import PyCsv` 

### Criar usuário:
    usuario = criarUsuario(
        givenName="Aluno",
        familyName="Campusero",
        primaryEmail='aluno.campusero@ufrrj.br',
        documento='1351654',
        curso='Direito',
        campus='RURAL',
        password='565464564654'
    )
    if usuario:
        loggerAdmin.add_row_csv([
            "Sucesso",
            usuario.get('id'),
            usuario.get('primaryEmail')
        ])
    else:
        loggerAdmin.add_row_csv([
            "Erro",
            usuario.get('primaryEmail')
        ])


# Drive 

### Lê arquivos no drive e compartilhando no classroom:
- importar:
    ```
        from modulos.classroom import criarAtividade
        from modulos.drive import listarArquivos, criarNoDrive
    ```
- uso:
    ```
        q="mimeType = 'application/vnd.google-apps.document' "
            arquivos = listarArquivos(q)
            arquivos
            for arquivo in arquivos:
                for turma in turmas:
                    body={
                        "title": "Atividade com novo link do drive",
                        "assigneeMode": "INDIVIDUAL_STUDENTS",
                        "maxPoints": 10,
                        "state": "PUBLISHED",
                        "description": "Testando",
                        "workType": "ASSIGNMENT",
                        "materials": [
                                        {
                                            "driveFile": {
                                                "driveFile": {
                                                    "id":arquivo.get('id') ,
                                                    "title": arquivo.get('name')
                                                    }
                                                },
                                                "shareMode": "VIEW"
                                        }

                                ]
                        }
                    criarAtividade(turma[1],body,"bot.script@ufrrj.br")

    ```


#DOCS

- Importar: `from modulos.documents import escreverDocumento`
- Escrever no docs:
    ```
        text1 =  "Olá pessoal, sejam muito bem vindos! "
        requests = [
            {
                'insertText': {
                    'location': {
                        'index': 1,
                    },
                    'text': text1
                }
            }
        ]

        escreverDocumento(
            driveId =novoarquivo.get("id"), 
            # driveId ="1o2_XbsqKFPoXCusNZKu1b00m1K0RVzC56BkD_w6MMZU", 
            requests=requests
        )

    ```

# Docker

### Implantação
- Instalar o docker
- No diretório deste projeto, rodar comando: `docker-compose up -d` or `docker-compose up`
- Será levantado:
    - Serviço adminer em `localhost:8080`
    - Banco postgres na porta `5432`

### Exemplo de uso dos serviços Docker

- Importar `from modulos.banco import logUsuariosNovos,criarTabelaLogUsuarios`
- Primeiro precisa criar a tabela, executando a função `criarTabelaLogUsuarios`, apenas uma vez
- Depois chama a função `logUsuariosNovos('31312312','usuario@ufrrj.br')`

- Analisem o arquivo banco dentro da pasta módulos
- Para que isso funcione o docker precisa estar rodando os serviços
- Vocês poderão visualizar os dados na tela do adminer em `localhost:8080`

<img src='/prints/p1.png'>

É isso, espero que tenham gostado!