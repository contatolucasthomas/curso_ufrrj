from pylib.googleadmin import authService
from decouple import config


escopos = [
    'https://www.googleapis.com/auth/admin.directory.user'
]
service = authService(
    escopos,
    "cred/{}".format(config("JSONFILE")),
    config('IMPERSONATED_EMAIL') #substituir pelo caminho do arquivo, cred/arquadasds.json
    ).getService("admin","directory_v1")

if __name__ == '__main__':

    body ={
        "name":{
            "givenName":"Carlos",
            "familyName":"Bastos"
        },
        "primaryEmail":"carlos.bastos@{}".format(config("DOMINIO")),
        "changePasswordAtNextLogin":"true",
        "password":"zxzx1212",
        "orgUnitPath":"/teste",
        "externalIds":[{
            "value":"6454654",
            "type":"organization"
        }],
        "organizations":[
            {
            "department":"direito",
            "costCenter":"rural"
            }
        ]
    }

    try:
        usuario = service.users().insert(body=body).execute()
        print(usuario)
    except Exception as e:
        print(e)
